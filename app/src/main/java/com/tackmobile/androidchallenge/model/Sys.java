package com.tackmobile.androidchallenge.model;

public class Sys {
    private int population;

    private String pod;

    public int getPopulation() {
        return this.population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public String getPod() {
        return this.pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }
}