package com.tackmobile.androidchallenge.model;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ForecastItem {
    private int dt;
    private Main main;
    private ArrayList<Weather> weather;
    private Clouds clouds;
    private Wind wind;
    private Sys sys;
    private String dt_txt;
    private Precipitation rain;
    private Precipitation snow;

    public int getDt() {
        return this.dt;
    }

    public void setDt(int dt) {
        this.dt = dt;
    }

    public Main getMain() {
        return this.main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public ArrayList<Weather> getWeather() {
        return this.weather;
    }

    public void setWeather(ArrayList<Weather> weather) {
        this.weather = weather;
    }

    public Clouds getClouds() {
        return this.clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public Wind getWind() {
        return this.wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Sys getSys() {
        return this.sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    public String getDtTxt() {
        String formatDate=dt_txt;
       String datePattern="EEE MMM dd,yyyy hh:mm";
        try {
            if(!dt_txt.matches(datePattern)) {
                Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(dt_txt);
                formatDate = new SimpleDateFormat("EEE MMM dd,yyyy hh:mm").format(date);
            }
        } catch (ParseException ex) {
         Log.d("Date Format",ex.getMessage());
        }
        finally {
            return formatDate;
        }
    }

    public void setDtTxt(String dt_txt) {

this.dt_txt=dt_txt;
    }

    public Precipitation getRain() {
        return this.rain;
    }

    public void setRain(Precipitation rain) {
        this.rain = rain;
    }

    public Precipitation getSnow() {
        return this.snow;
    }

    public void setSnow(Precipitation snow) {
        this.snow = snow;
    }
}
