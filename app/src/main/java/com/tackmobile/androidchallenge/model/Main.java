package com.tackmobile.androidchallenge.model;

public class Main {
    private double temp;
    private double temp_min;
    private double temp_max;
    private double pressure;
    private double sea_level;
    private double grnd_level;
    private int humidity;
    private double temp_kf;

    public double getTemp() {
        return this.temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public double getTempMin() {
        return this.temp_min;
    }

    public void setTempMin(double temp_min) {
        this.temp_min = temp_min;
    }

    public double getTempMax() {
        return this.temp_max;
    }

    public void setTempMax(double temp_max) {
        this.temp_max = temp_max;
    }

    public double getPressure() {
        return this.pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public double getSeaLevel() {
        return this.sea_level;
    }

    public void setSeaLevel(double sea_level) {
        this.sea_level = sea_level;
    }

    public double getGrndLevel() {
        return this.grnd_level;
    }

    public void setGrndLevel(double grnd_level) {
        this.grnd_level = grnd_level;
    }

    public int getHumidity() {
        return this.humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public double getTempKf() {
        return this.temp_kf;
    }

    public void setTempKf(double temp_kf) {
        this.temp_kf = temp_kf;
    }
}
