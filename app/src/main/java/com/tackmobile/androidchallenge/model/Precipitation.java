package com.tackmobile.androidchallenge.model;

public class Precipitation {
    private double threeHour;

    public double getThreeHour() {
        return this.threeHour;
    }

    public void setThreeHour(double threeHour) {
        this.threeHour = threeHour;
    }
}
