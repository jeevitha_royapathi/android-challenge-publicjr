package com.tackmobile.androidchallenge.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tackmobile.androidchallenge.Helpers.Utility;
import com.tackmobile.androidchallenge.R;
import com.tackmobile.androidchallenge.fragments.Forecast_Fragment;
import com.tackmobile.androidchallenge.model.ForecastItem;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder> {
    List<ForecastItem> items = new ArrayList<>();
    private Context context;
    public LruCache<String, Bitmap> mImgMemoryCache;
    final  private String imageUrl="http://openweathermap.org/img/w/";
    Forecast_Fragment.OnItemClickListener mItemClickListener;
    public ForecastAdapter(Context context,LruCache<String, Bitmap> imgMemoryCache) {
        this.context = context;
        this.mImgMemoryCache=imgMemoryCache;
    }
    public  List<ForecastItem>  getItems()
    {
        return items;
    }

    @Override
    public ForecastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ForecastViewHolder(
                LayoutInflater.from(context).inflate(
                        R.layout.view_holder_forecast,
                        parent,
                        false
                )
        );
    }
    public void setOnItemClickListener(Forecast_Fragment.OnItemClickListener onItemClickListener)
    {
        mItemClickListener=onItemClickListener;
    }
    @Override
    public void onBindViewHolder(ForecastViewHolder holder, int position) {
        ForecastItem forecastItem = items.get(position);
        holder.time.setText(forecastItem.getDtTxt());
        DecimalFormat f = new DecimalFormat("##.00");
        if(Utility.isMetric(context))
            holder.temp.setText(String.valueOf(f.format((forecastItem.getMain().getTemp()-32)*5/9)));
        else
            holder.temp.setText(String.valueOf(forecastItem.getMain().getTemp()));
        holder.description.setText(forecastItem.getWeather().get(0).getDescription());
        Bitmap bitmap=null ;
        String tempUrl=imageUrl+forecastItem.getWeather().get(0).getIcon()+".png";

        if(mImgMemoryCache!= null)
            bitmap= mImgMemoryCache.get(tempUrl);
        if (bitmap != null) {
            holder.imageIcon.setImageBitmap(bitmap);
        } else {
            //download image from the url

            MyDownloadImageAsyncTask task = new MyDownloadImageAsyncTask(holder.imageIcon);
            task.execute(new String[]{tempUrl});
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void updateItems(ArrayList<ForecastItem> items) {
           this.items = items;
    }

    public class ForecastViewHolder
            extends RecyclerView.ViewHolder {

        @Bind(R.id.imgIcon)
        ImageView imageIcon;
        @Bind(R.id.forecast_time)
        TextView time;
        @Bind(R.id.forecast_temp)
        TextView temp;
        @Bind(R.id.forecast_description)
        TextView description;

        public ForecastViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v)
                {
                    if(mItemClickListener != null)
                        try {
                            mItemClickListener.onItemClick(items.get(getLayoutPosition()));
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        }
                }

            });
        }
    }

    //function to download the image and update cache
    private  class MyDownloadImageAsyncTask extends AsyncTask<String,Void,Bitmap>
    {

        private  final WeakReference<ImageView> imageViewReference;
        public MyDownloadImageAsyncTask(ImageView imv)
        {

            imageViewReference = new WeakReference<ImageView>(imv);
        }
        @Override
        protected  Bitmap doInBackground(String... urls)
        {
            Bitmap bitmap=null;
            if(isNetworkAvailable()) {
                for (String url : urls) {
                    //calling utility function to download image
                    bitmap = Utility.downloadImage(url);
                    if (bitmap != null && mImgMemoryCache != null)
                        mImgMemoryCache.put(url, bitmap);// add it to the cache after download

                }
            }
            return  bitmap;

        }

        @Override
        protected  void  onPostExecute(Bitmap bitmap)
        {
            // checking whether imageView reference still exits and setting downloaded image to it
            if(imageViewReference != null)
            {

                final  ImageView imageView=imageViewReference.get();
                if(imageView!=null)
                {
                    if(bitmap != null)
                        imageView.setImageBitmap(bitmap);
                }

            }

        }
    }
    //Function to check NextWorkAvailablity
    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        // if no network is available networkInfo will be null
        // otherwise check if we are connected
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

}
