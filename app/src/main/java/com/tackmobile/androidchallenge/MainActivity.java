package com.tackmobile.androidchallenge;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.tackmobile.androidchallenge.Helpers.Utility;
import com.tackmobile.androidchallenge.fragments.ForecastDetail_Fragment;
import com.tackmobile.androidchallenge.fragments.Forecast_Fragment;
import com.tackmobile.androidchallenge.model.ForecastItem;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements Forecast_Fragment.OnItemClickListener {
    @Bind(R.id.toolbar_layout)
    CollapsingToolbarLayout collapsingToolbarLayout;
    private String mLocation;
    private Boolean mIsMetric;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLocation = Utility.getPreferredLocation(this).toUpperCase();
        mIsMetric=Utility.isMetric(this);
        if(savedInstanceState==null)
        {
            Forecast_Fragment newFragment= Forecast_Fragment.newInstance();
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.card_flip_left_in,
                    R.anim.card_flip_left_out,
                    R.anim.card_flip_right_in,
                    R.anim.card_flip_right_out);
            ft.add(R.id.container, newFragment,"ForeCast");
            ft.commit();
        }
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        collapsingToolbarLayout.setTitle(mLocation);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // This can be used to preserve the state when orientation changes
    // TODO: I have created fragment to display forecast and forecastdetail
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }


    @Override
    protected void onResume() {
        super.onResume();
        String location = Utility.getPreferredLocation(this).toUpperCase();
        // update the location in our second pane using the fragment manager
        if ((location != null && !location.equals(mLocation))) {
            mLocation = location;
            collapsingToolbarLayout.setTitle(mLocation);
            Forecast_Fragment newFragment= Forecast_Fragment.newInstance();
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.card_flip_left_in,
                    R.anim.card_flip_left_out,
                    R.anim.card_flip_right_in,
                    R.anim.card_flip_right_out);
            ft.replace(R.id.container, newFragment,"ForeCast");

            ft.commit();
          }
        else if(mIsMetric != Utility.isMetric(this))
        {
            mIsMetric=Utility.isMetric(this);
            Forecast_Fragment newFragment=(Forecast_Fragment) getSupportFragmentManager().findFragmentByTag("ForeCast");
            if(newFragment != null && newFragment.isVisible())
            {
                newFragment.ResetUnits(mIsMetric);
            }
        }
    }

    @Override
    public void onItemClick(ForecastItem forecastItem) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String forecastDetails=ow.writeValueAsString(forecastItem);
        ForecastDetail_Fragment newFragment= ForecastDetail_Fragment.newInstance(forecastDetails);
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.card_flip_left_in,
                R.anim.card_flip_left_out,
                R.anim.card_flip_right_in,
                R.anim.card_flip_right_out);
        ft.replace(R.id.container, newFragment,"ForeCastDetail");
        ft.addToBackStack(null);
        ft.commit();
    }
}






