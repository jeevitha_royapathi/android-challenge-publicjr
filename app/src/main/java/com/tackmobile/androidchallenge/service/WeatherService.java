package com.tackmobile.androidchallenge.service;

import com.tackmobile.androidchallenge.model.RootObject;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface WeatherService {
    @GET("forecast")
    Observable<RootObject> getForecast(@Query("q") String city);

}