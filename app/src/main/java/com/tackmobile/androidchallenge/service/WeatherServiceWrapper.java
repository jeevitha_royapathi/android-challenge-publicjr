package com.tackmobile.androidchallenge.service;

import android.graphics.Bitmap;

import com.tackmobile.androidchallenge.BuildConfig;
import com.tackmobile.androidchallenge.model.RootObject;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;
import retrofit2.RxJavaCallAdapterFactory;
import rx.Observable;

public class WeatherServiceWrapper implements IWeatherServiceWrapper {

    /**
     * Update your API_KEY value in gradle-local.properties in the root folder
     */
    private static final String KEY = BuildConfig.API_KEY;
    private  String Metric;
    private static IWeatherServiceWrapper instance = null;
    private final WeatherService service;
    private final ImageService imageService;

    private WeatherServiceWrapper() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                HttpUrl url = request
                        .url()
                        .newBuilder()
                        .addQueryParameter("appid", KEY)
                        .addQueryParameter("mode", "json")
                        .addQueryParameter("units", "imperial")
                        .build();

                request = request.newBuilder().url(url).build();
                return chain.proceed(request);
            }
        });

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        builder.addInterceptor(loggingInterceptor);

        OkHttpClient okHttpClient = builder.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/data/2.5/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        service = retrofit.create(WeatherService.class);

        Retrofit retrofitImg = new Retrofit.Builder()
                .baseUrl("http://openweathermap.org/img/w/")
                .build();
        imageService =retrofitImg.create(ImageService.class);

    }

    public static IWeatherServiceWrapper getInstance() {
        if (instance == null) {
            instance = new WeatherServiceWrapper();
        }
        return instance;
    }

    @Override
    public Observable<RootObject> getForecast(String city) {
        if (service != null) {
            return service.getForecast(city);
        } else {
            throw new IllegalArgumentException("Service not initialized");
        }
    }

    @Override
    public Observable<Bitmap> getIcon(String icon)
    {
        if (imageService != null) {
            return imageService.getIcon(icon);
        } else {
            throw new IllegalArgumentException("Service not initialized");
        }
    }
}
