package com.tackmobile.androidchallenge.service;


import android.graphics.Bitmap;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;
/**
 * Created by jeevitha on 2/6/2016.
 */
public interface ImageService
{
    @GET("icon")
    Observable<Bitmap> getIcon(@Path("icon") String icon);
}