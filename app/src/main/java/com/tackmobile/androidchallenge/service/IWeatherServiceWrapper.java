package com.tackmobile.androidchallenge.service;

import android.graphics.Bitmap;

import com.tackmobile.androidchallenge.model.RootObject;

import rx.Observable;

public interface IWeatherServiceWrapper {
    Observable<RootObject> getForecast(String city);
    Observable<Bitmap> getIcon(String icon);
}
