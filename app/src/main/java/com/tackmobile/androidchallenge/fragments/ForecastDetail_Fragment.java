package com.tackmobile.androidchallenge.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tackmobile.androidchallenge.Helpers.Utility;
import com.tackmobile.androidchallenge.R;
import com.tackmobile.androidchallenge.model.ForecastItem;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.DecimalFormat;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ForecastDetail_Fragment extends Fragment {
    private static final String ARG_DETAIL = "DETAILS";
    private String ForeCastDetails;
    @Bind(R.id.imgIcon)
    ImageView imgIcon;
    @Bind(R.id.txtWeatherDetail)
    TextView txtWeatherDesc;
    @Bind(R.id.txtHumidity)
    TextView txtHumidity;
    @Bind(R.id.txtMinTemp)
    TextView txtMinTemp;
    @Bind(R.id.txtMaxTemp)
    TextView txtMaxTemp;
    @Bind(R.id.txtPressure)
    TextView txtPressure;
    @Bind(R.id.txtDate)
    TextView txtDate;
    final  private String imageUrl="http://openweathermap.org/img/w/";
    public ForecastDetail_Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    public static ForecastDetail_Fragment newInstance(String foreCastDetails) {
        ForecastDetail_Fragment fragment = new ForecastDetail_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_DETAIL, foreCastDetails);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ForeCastDetails = getArguments().getString(ARG_DETAIL);

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_forecast_detail_, container, false);
        try {

            ButterKnife.bind(this,rootView);
            DecimalFormat f = new DecimalFormat("##.00");
            ObjectMapper om = new ObjectMapper();
            ForecastItem mForecastItem = om.readValue(ForeCastDetails, ForecastItem.class);
            txtWeatherDesc.setText(mForecastItem.getWeather().get(0).getDescription());
           txtHumidity.setText(String.valueOf(mForecastItem.getMain().getHumidity()));
            txtPressure.setText(String.valueOf(mForecastItem.getMain().getPressure()));
            if(Utility.isMetric(getActivity())) {

                txtMinTemp.setText(String.valueOf(f.format((mForecastItem.getMain().getTempMin() - 32) * 5 / 9)));
                txtMaxTemp.setText(String.valueOf(f.format((mForecastItem.getMain().getTempMax() - 32) * 5 / 9)));

            }else {

                txtMinTemp.setText(String.valueOf(mForecastItem.getMain().getTempMin()));
                txtMaxTemp.setText(String.valueOf(mForecastItem.getMain().getTempMax()));

            }
            txtDate.setText(mForecastItem.getDtTxt());
            MyDownloadImageAsyncTask task = new MyDownloadImageAsyncTask((imgIcon));
            String tempUrl=imageUrl+mForecastItem.getWeather().get(0).getIcon()+".png";
            task.execute(new String[]{tempUrl});
        } catch (IOException e) {
            Log.d("Frag Details", "onCreateView: "+e.getMessage());;
        }
        return  rootView;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }
    //Function to download the image
    private  class MyDownloadImageAsyncTask extends AsyncTask<String,Void,Bitmap>
    {
        //Creating Weak Reference to not to affect garbage collector to collect it
        private  final WeakReference<ImageView> imageViewReference;
        public MyDownloadImageAsyncTask(ImageView imv)
        {
            imageViewReference = new WeakReference<ImageView>(imv);
        }
        @Override
        protected  Bitmap doInBackground(String... urls)
        {
            Bitmap bitmap=null;

            for (String url :urls)
            {
                //Calling utility function to download the image
                bitmap= Utility.downloadImage(url);
            }
            return  bitmap;

        }

        @Override
        protected  void  onPostExecute(Bitmap bitmap)
        {
            //set the image only if it is not collected by garbage i.e only it is active
            if(imageViewReference != null && bitmap != null)
            {
                final  ImageView imageView=imageViewReference.get();
                if(imageView!=null)
                    imageView.setImageBitmap(bitmap);
            }

        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
    }

}
