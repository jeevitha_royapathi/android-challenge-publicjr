package com.tackmobile.androidchallenge.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tackmobile.androidchallenge.R;
import com.tackmobile.androidchallenge.adapter.ForecastAdapter;
import com.tackmobile.androidchallenge.model.ForecastItem;
import com.tackmobile.androidchallenge.model.RootObject;
import com.tackmobile.androidchallenge.service.IWeatherServiceWrapper;
import com.tackmobile.androidchallenge.service.WeatherServiceWrapper;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class Forecast_Fragment extends Fragment {

    private IWeatherServiceWrapper weatherService;
    private ForecastAdapter forecastAdapter;
    private static String CITY="SanDiego";
    private OnItemClickListener mOnItemClickListener;
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    public LruCache<String, Bitmap> mImgMemoryCache;
    private Boolean misMetric;

    public Forecast_Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment Forecast_Fragment.
     */
    @OnClick(R.id.fab)
    public void onFabClick() {
       updateWeather();
    }


    public static Forecast_Fragment newInstance() {
        Forecast_Fragment fragment = new Forecast_Fragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //retain this fragment , so it won't be destroyed when the configuration changes
        setRetainInstance(true);

        if (mImgMemoryCache == null) {

            //Use 1/8th of the available memory for this memory cache
            final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
            mImgMemoryCache = new LruCache<String, Bitmap>((int) maxMemory / 8) {
                @Override
                protected int sizeOf(String key, Bitmap bitmap) {
                    //cache size will be measured in kilobytes rather than the number of items
                    return bitmap.getByteCount() / 1024;
                }
            };
        }
        weatherService = WeatherServiceWrapper.getInstance();
        forecastAdapter = new ForecastAdapter(getContext(),mImgMemoryCache);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView=inflater.inflate(R.layout.fragment_forecast_,container,false);
        ButterKnife.bind(this,rootView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        forecastAdapter.setOnItemClickListener(mOnItemClickListener);
        recyclerView.setAdapter(forecastAdapter);
        updateWeather();
        return rootView;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mOnItemClickListener=(OnItemClickListener)getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mOnItemClickListener = null;
    }


    private void updateWeather() {
        weatherService.getForecast(Utility.getPreferredLocation(getContext()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<RootObject>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(getActivity().getClass().getSimpleName(), "Error", e);
                    }

                    @Override
                    public void onNext(RootObject rootObject) {
                        forecastAdapter.updateItems(rootObject.getList());
                        forecastAdapter.notifyDataSetChanged();
                    }
                });

    }

    public void ResetUnits(Boolean isMetric) {
        misMetric = isMetric;
        if (forecastAdapter != null) {
            forecastAdapter.notifyDataSetChanged();
        }
    }
    //Interface to handle recylerview onClick Events
    public interface OnItemClickListener {
        public void onItemClick(ForecastItem forecastItem) throws JsonProcessingException;
    }
}
