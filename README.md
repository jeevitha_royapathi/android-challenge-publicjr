# Tack Android Challenge #

Weather is everywhere, but sometimes you're not sure if it's hot or cold outside, right?

Well, the client couldn't find an app that made her happy, so she had one made. She has provided us with her nephew's weather app that needs some... improvements.
It looks like he's given us the service we must use, and some models for mapping the Json, so that's helpful.
Right now, the app just requests weather in San Diego, but the client's grandparents live in Australia, so it's not 
helpful for them. The client has a list of features she would like added, and has said we can use as little or as much 
of the provided code as we want.

### Getting Started ###

* Make sure your Android Studio is using the latest Canary build (currently 2.0 Preview 9)
* Go to http://openweathermap.org/ and make an account to get an API key. 
* Copy `gradle-local.properties.placeholder` to `gradle-local.properties`
* Update the `API_KEY` property in `gradle-local.properties` with your key from openweathermap.org

### Service docs ###

* Forecast response sample and definitions:
http://openweathermap.org/forecast5#JSON

* Weather conditions/icons
http://openweathermap.org/weather-conditions

### Demo day is in a fews days. Here are the tickets we've prioritized for this sprint ###

* BUG: Broken build! The project can't be compiled on some dev's workstations. What's going on!?
* Feature: Make city editable. As a user, when I select a menu item to update location, I can input my city and request data for that city
* Feature: Change unit of measurement. As a user, when I select a menu item, I can change my unit of measurement to either Celsius or Fahrenheit
* Feature: Add weather icon to status view. As a user, when I view the weather, I should see an icon representing the current weather conditions
* Feature: Date should be in format Tue Jan 1, 2016 13:00. As a user, I want the date to be in a more readable format
* Feature: Selecting an item in the list, should open a detailed view showing min and max temp, humidity, pressure, weather description, and icon. As a user, I want to view the details of a forecast when I select the overview item.
* BUG: Rotation loses data. Date in the list should be preserved through rotation

### Delivery ###

* Fork this project and push your commits to your fork project.
* Create a Pull Request on BitBucket requesting to merge your forked project branch to this project's master branch.